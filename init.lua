-- ANTI GRIEF by rnd
-- Copyright 2016 rnd
-- modified by SaKeL for Survival X Server

----------------------------------------------------------------------------
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
----------------------------------------------------------------------------


-- prevents placing of lava/liquid sources or buckets above 0


--water
--bucket:bucket_water

function prevent_place_above(name)

	local old_on_place = minetest.registered_craftitems[name] -- on_place
	local old_after_place_node = minetest.registered_nodes[name] --after_place_node

	-- for items (buckets)
	if old_on_place and old_on_place.on_place then

		old_on_place = old_on_place.on_place

		minetest.registered_craftitems[name].on_place = function(itemstack, placer, pointed_thing)
			local pos = pointed_thing.above
			local placer_name = placer:get_player_name()
			local item_name = string.split(itemstack:get_name(), ":")[2]
			local max_y = 50 -- water

			if string.find(itemstack:get_name(), "lava") then
				max_y = 0 -- lava
			end

			if pos.y > max_y and placer_name ~= "ADMIN" and placer_name ~= "SaKeL" then
				minetest.log("action","ANTI GRIEF " .. placer_name .. " tried to place " .. name .. " at " .. minetest.pos_to_string(pos))

				minetest.chat_send_player(placer_name, "You can place "..item_name.." only below "..max_y.." blocks")
				return itemstack
			else
				return old_on_place(itemstack, placer, pointed_thing)
			end
		end

		return
	end

	-- for nodes (water_source block)
	if old_after_place_node then
		old_after_place_node = old_after_place_node.after_place_node

		local table = minetest.registered_nodes[name]
		local table2 = {}

		for i,v in pairs(table) do
			table2[i] = v
		end

		table2.after_place_node = function(pos, placer, itemstack, pointed_thing)

			local placer_name = placer:get_player_name()
			local item_name = string.split(itemstack:get_name(), ":")[2]
			local max_y = 50 -- water

			if string.find(itemstack:get_name(), "lava") then
				max_y = 0 -- lava
			end

			if string.find(itemstack:get_name(), "tnt") then
				max_y = -150 -- tnt
			end

			if pos.y > max_y and placer_name ~= "ADMIN" and placer_name ~= "SaKeL" then
				minetest.log("action","ANTI GRIEF " .. placer_name .. " tried to place " .. name .. " at " .. minetest.pos_to_string(pos))

				minetest.chat_send_player(placer_name, "You can place "..item_name.." only below "..max_y.." blocks")

				minetest.set_node(pos, {name = "air"})
				return itemstack
			end
		end

		minetest.register_node(":"..name, table2)
		return
	end

	return
end

-- prevent TNT explosion
minetest.after(0, function ()
	local old_on_timer = minetest.registered_nodes["tnt:tnt_burning"]

	if old_on_timer and old_on_timer.on_timer then
		old_on_timer = old_on_timer.on_timer

		minetest.registered_nodes["tnt:tnt_burning"].on_timer = function(pos, elapsed)

			local meta = minetest.get_meta(pos)
			local owner = meta:get_string("owner")

			if not owner then
				owner = ""
			end

			if pos.y > -150 and owner ~= "ADMIN" and owner ~= "SaKeL" then
				minetest.sound_play("antigrief_fart", {pos = pos, gain = 1.5, max_hear_distance = 2*64})
				minetest.remove_node(pos)

				-- add effect
				minetest.add_particlespawner({
					amount = 64,
					time = 0.5,
					minpos = vector.subtract(pos, 1 / 2),
					maxpos = vector.add(pos, 1 / 2),
					minvel = {x = -10, y = -10, z = -10},
					maxvel = {x = 10, y = 10, z = 10},
					minacc = vector.new(),
					maxacc = vector.new(),
					minexptime = 1,
					maxexptime = 2.5,
					minsize = 1 * 3,
					maxsize = 1 * 5,
					texture = "tnt_smoke.png",
				})
			else
				return old_on_timer(pos, elapsed)
			end
		
		end
	end
end)

minetest.after(0, function ()
	prevent_place_above("bucket:bucket_water")
	prevent_place_above("bucket:bucket_river_water")
	prevent_place_above("default:water_source")
	prevent_place_above("default:river_water_source")
	prevent_place_above("bucket:bucket_lava")
	prevent_place_above("default:lava_source")
	prevent_place_above("tnt:tnt")
	prevent_place_above("tnt:gunpowder")
end)